package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull String argument) {
        super("***Error: Argument " + argument + " Not Found***");
    }

}
