package com.ushakova.tm.api;

import com.ushakova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
