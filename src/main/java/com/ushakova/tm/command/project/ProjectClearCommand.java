package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Clear all projects.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("*Project Clear*");
        serviceLocator.getProjectService().clear(userId);
    }

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
