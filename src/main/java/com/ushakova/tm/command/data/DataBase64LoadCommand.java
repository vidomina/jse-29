package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Get data from BASE64 file.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String base64Data = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte[] decodedData = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @NotNull
    public String name() {
        return "data-get-base64";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
